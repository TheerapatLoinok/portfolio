import React from 'react'
import Head from 'next/head'
import Header from '../components/Header/Header'
import MainTopic from '../components/MainTopic/MainTopic'
import styles from './LayoutStyles.module.css'
import Footer from '../components/Footer/Footer'

export default function Layout({children}) {
  return (
    <div className ={styles.Container}>
      <Header></Header>
      {children}
      <Footer></Footer>
    </div>
  )
}
