export const projects = [
    {
        id: 0,
        title: 'PHP',
        description:'Hi',
        image:'/images/php.png',
        link: 'https://www.php.net/',
    },
    {
        id: 1,
        title: 'Laravel',
        description:'Hi',
        image:'/images/laravel.png',
        link: 'https://laravel.com/',
    },
    {
        id: 2,
        title: 'VueJS',
        description:'Hi',
        image:'/images/vue.png',
        link: 'https://vuejs.org/',
    },
    {
        id: 3,
        title: 'NuxtJS',
        description:'Hi',
        image:'/images/nuxt.png',
        link: 'https://nuxtjs.org/',
    },
    {
        id: 4,
        title: 'ReactJS',
        description:'Hi',
        image:'/images/react.png',
        link: 'https://reactjs.org/',
    },
    {
        id: 5,
        title: 'NextJS',
        description:'Hi',
        image:'/images/next.png',
        link: 'https://nextjs.org/',
    },
  ];