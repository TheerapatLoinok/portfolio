import React from 'react'
import Link from 'next/link'
import Styles from './MainTopic.module.css'
export default function MainTopic() {
  return (
    <div className={Styles.Section}>
      <div className={Styles.SectionTitle}>
        Welcome to <br/> 
        My Personal Portfolio
      </div>
      <div className={Styles.SectionText}>
        I am a KhonKaen University student, studying in College of Computing, Computer Science department. Currently, I am studying on website and application development, and I have had experiences from cooperative as well. Working well with other people is one of my advantages, 
        I do respect and like to listen other people opinions, and adapt it very quick. Working under pressure is not a matter of concern for me as well. Importantly, I am always ready to develop myself and very open in learning new things.
      </div>
    </div>
  )
}
