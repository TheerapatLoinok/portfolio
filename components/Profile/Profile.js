import React from 'react'
import Styles from './Profile.module.css'
import { GoPerson } from "react-icons/go"
import Image from 'next/image'

export default function Profile() {
  return (
    <div className={Styles.Section}>
      <div className={Styles.SectionTitle}>
        Profile
      </div>
      <div className={Styles.Card}>
      <div className={Styles.SectionText}>
        <div className={Styles.Row}>
            <div className={Styles.Column}>
                <div className ={Styles.profileImage}>
                <Image 
                  src= 'https://scontent.fkkc3-1.fna.fbcdn.net/v/t39.30808-6/279732354_1084773375405698_6921803509567009698_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeFiUfh-1Jt-8KazxLhZ3H9uUol1K_ocIOZSiXUr-hwg5tkLvvZo7tY56r2FDwDyU_WMW3ev57aIZ8bX3d4UCVQU&_nc_ohc=ZVAQ1UQazqEAX8i0YyE&_nc_ht=scontent.fkkc3-1.fna&oh=00_AfCXBMmkIqFCszCfnEx1n3CozfDXpOXUzYvdllbG3zU7Lg&oe=635FFF8B'
                  alt='profile image'
                  width= '220px'
                  height = '300px'
                />
                </div>
            </div>
            <div className={Styles.Column}>
              <div className={Styles.Text}>
                  Name: Theerapat Loinok <br/>
                  Nickname: Beer <br/>
                  Department: Computer Science <br/>
                  University: Khonkean University<br/>
                  Email: Theerapat Loinok<br/>
                  Telephone: 082-556-0679 <br/>
              </div>
            </div>
        </div>
      </div>
      </div>
    </div>
  )
}
