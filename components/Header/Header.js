import React from 'react'
import Styles from './HeaderStyles.module.css'
import NavBar from '../NavigationBar/NavBar'
export default function Header() {
  return (
    <div class={Styles.Container}>
      <NavBar></NavBar>
    </div>
  )
}
