import React from 'react'
import Link from 'next/link'
import Styles from './NavBarStyles.module.css'
export default function NavBar() {
  return (
    <div>
        <ul className ={Styles.Navul}>
            <li className = {Styles.Navli1}><Link href="/"><a  className = {Styles.Nava}><span>Portfolio</span></a></Link></li>
            <li className = {Styles.Navli1}><Link href="/profile"><a  className = {Styles.Nava}><span>Profile</span></a></Link></li>
            <li className = {Styles.Navli1}><Link href="/project"><a  className = {Styles.Nava}><span>Project</span></a></Link></li>
            <li className = {Styles.Navli1}><Link href="/skill"><a  className = {Styles.Nava}><span>Skill</span></a></Link></li>
            
            <li className = {Styles.Navli2}><Link href="https://github.com/ "><a  className = {Styles.Nava} target="_blank"><span>GitHub</span></a></Link></li>
            <li className = {Styles.Navli2}><Link href="https://www.linkedin.com/"><a  className = {Styles.Nava} target="_blank"><span>LinkedIn</span></a></Link></li>
            
        </ul>
    </div>
  )
}
