import React from 'react'
import Styles from './Footer.module.css'
import { AiOutlineMail } from "react-icons/ai"
import { BsFillTelephoneFill } from "react-icons/bs"
import { GoPerson } from "react-icons/go"


export default function Footer() {
  return (
    <div class={Styles.Container}>
      <div class={Styles.TextTitle}>
        ContacUS
      </div>
      <div class={Styles.Text}>
      <AiOutlineMail size="1rem"/> Theerapat.l@kkumail.com <br/>  
      <GoPerson size="1rem"/> Theerapat Loinok <br/>
      <BsFillTelephoneFill size="1rem"/> 0825560679 <br/>
      </div>
      <div class = {Styles.Coppyright}>
        Created by Theerapat Loinok
      </div>
    </div>
  )
}
