import React from "react";
import Styles from "./Skill.module.css";
import { projects } from "../../constants/constants";
import Image from "next/image";
import Link from "next/link";

export default function Skill() {
  return (
    <div className={Styles.Section}>
      <div className={Styles.SectionTitle}>Skill</div>
      <div className={Styles.GridContainer}>
        {projects.map((p, i) => {
          return (
            <div className={Styles.BlogCard} key={i}>
              <div className={Styles.SectionText}>
                <div className={Styles.TextTitle}>{p.title}</div>
                <div>
                  <Image
                    src={p.image}
                    alt="skill image"
                    width={200}
                    height={180}
                  />
                </div>
                <div className={Styles.button}>
                  <Link href={p.link} ><a target='_blank'>Click for view page</a></Link>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
