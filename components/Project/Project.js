import React from "react";
import Styles from "./Project.module.css";
import Image from 'next/image'
export default function Project() {
  return (
    <div className={Styles.Section}>
      <div className={Styles.SectionTitle}>Project</div>
      <div className={Styles.SectionText}>
        <div className={Styles.Row}>
          <div className={Styles.Column}>
            <h3>Develop IOS Application (THESIS)</h3>
            Developed exercise app which is able to track users body performance by using Swift and Xcode
          </div>
          <div className={Styles.Column}>
            <div className={Styles.projectImage}>
            <Image 
                  src= 'https://i.ytimg.com/vi/f86K8h-8C9A/maxresdefault.jpg'
                  alt='profile image'
                  width={350}
                  height={230}
                />
            </div>
          </div>
        </div>
        <div className={Styles.Row}>
          <div className={Styles.Column}>
            <h3>INTERNSHIP EXPERIENCE</h3>
            Develop Customer Relationship Management system (CRM) by using Laravel and Nuxt to facilitate the operation department.
          </div>
          <div className={Styles.Column}>
            <div className={Styles.projectImage}>
            <Image 
                  src= 'https://ddq1mzvkg4fn7.cloudfront.net/2021/01/--.png'
                  alt='profile image'
                  width={350}
                  height={230}
                />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
