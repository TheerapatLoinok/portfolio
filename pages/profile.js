import React from 'react'
import Head from 'next/head'
import Layout from '../layout/Layout'
import Profile from '../components/Profile/Profile'
export default function profile() {
  return (
    <>
     <Head>
        <meta name="description" content="Portfolio" />
        <title>Profile</title>
        <link rel="icon" href="https://cdn-icons-png.flaticon.com/512/25/25634.png" />
      </Head>
      <Layout>
        <Profile></Profile>
      </Layout>
    </>
  )
}
