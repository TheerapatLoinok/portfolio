import React from 'react'
import Head from 'next/head'
import Layout from '../layout/Layout'
import Skill from '../components/Skill/Skill'
export default function skill() {
  return (
    <>
     <Head>
        <meta name="description" content="Portfolio" />
        <title>Project</title>
        <link rel="icon" href="https://cdn-icons-png.flaticon.com/512/25/25634.png" />
      </Head>
      <Layout>
        <Skill></Skill>
      </Layout>
    </>
  )
}
