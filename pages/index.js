import Layout from '../layout/Layout'
import Head from 'next/head'
import MainTopic from '../components/MainTopic/MainTopic'
import Profile from '../components/Profile/Profile'
import Project from '../components/Project/Project'
import Skill from '../components/Skill/Skill'

export default function Home() {
  return (
    <>
      <Head>
        <meta name="description" content="Portfolio" />
        <title>Portfolio</title>
        <link rel="icon" href="https://cdn-icons-png.flaticon.com/512/25/25634.png" />
      </Head>
      <Layout>
        <MainTopic></MainTopic>
      </Layout>
    </>
  )
}
